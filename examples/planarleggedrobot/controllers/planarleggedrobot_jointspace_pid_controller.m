% proNEu: A tool for rigid multi-body mechanics in robotics.
% 
% Copyright (C) 2017  Marco Hutter, Christian Gehring, C. Dario Bellicoso,
% Vassilios Tsounis
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%
%   File:           monopod_jointspace_pid_controller.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           1/2/2017
%
%   Desciption:     Joint-space PID motion controller.
%

function [ T_j ] = planarleggedrobot_jointspace_pid_controller(model,world,logger,t,x)
    
    % Integrators
    persistent integrator_Hl;
    persistent integrator_Kl;
    persistent integrator_Hr;
    persistent integrator_Kr;

    % Set hip gains left
    K_Hl_P = 5.0e-2/10;
    K_Hl_I = 1.0e-1/10;
    
    % Set knee gains left
    K_Kl_P = 1.0e-3/10;
    K_Kl_I = 5.0e-1/10;
    
    % Set hip gains right
    K_Hr_P = 5.0e-2/10;
    K_Hr_I = 1.0e-1/10;
    
    % Set knee gains right
    K_Kr_P = 1.0e-3/10;
    K_Kr_I = 5.0e-1/10;
    
    % Get measurements
    dq_Hl = x(7+4); 
    dq_Kl = x(7+5);
    dq_Hr = x(7+6);
    dq_Kr = x(7+7);
    
    % Reset integrators
    if t <= 0.05
        integrator_Hl = 0;
        integrator_Kl = 0;
        integrator_Hr = 0;
        integrator_Kr = 0;
    end
    
    % Set desired
    dq_Hld = 0.0;
    dq_Kld = 0.0;
    dq_Hrd = 0.0;
    dq_Krd = 0.0;
    
    % HIP Freeze Controller left
    velerror_Hl = dq_Hld - dq_Hl;
    integrator_Hl = integrator_Hl + K_Hl_I*velerror_Hl;
    T_HFEl = K_Hl_P*velerror_Hl + integrator_Hl;
    
    % KNEE Freeze Controller left
    velerror_Kl = dq_Kld - dq_Kl;
    integrator_Kl = integrator_Kl + K_Kl_I*velerror_Kl;
    T_KFEl = K_Kl_P*velerror_Kl + integrator_Kl;
    
    
    % HIP Freeze Controller left
    velerror_Hr = dq_Hrd - dq_Hr;
    integrator_Hr = integrator_Hr + K_Hr_I*velerror_Hr;
    T_HFEr = K_Hr_P*velerror_Hr + integrator_Hr;
    
    % KNEE Freeze Controller left
    velerror_Kr = dq_Krd - dq_Kr;
    integrator_Kr = integrator_Kr + K_Kr_I*velerror_Kr;
    T_KFEr = K_Kr_P*velerror_Kr + integrator_Kr;
    
    % Compose joint torques vector
    T_j = [T_HFEl T_KFEl T_HFEr T_KFEr].';
    
end
