% proNEu: A tool for rigid multi-body mechanics in robotics.
%
% Copyright (C) 2017  Marco Hutter, Christian Gehring, C. Dario Bellicoso,
% Vassilios Tsounis
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%
%   File:           planarleggedrobot_taskspace_pid_controller.m
%
%   Authors:        Manuel Breitenstein, bmanuel@ethz.ch
%
%   Date:           27/10/2017
%
%   Desciption:     Task-space PID controller.
%

function [ T_j ] = Copy_of_planarleggedrobot_taskspace_pid_controller(model,world,logger,t,x)
    % Number of bodies
    n=7;
    
    % Number of body coordinates
    nb=3;
    
    % Number of Task space coordinates
    nTs=3;
        
    %Set gains
    %K= diag([K_theta,   K_hl,    K_hr ]
    KP = diag([1.0e+2 ,1.0e+3 , 1.0e+3].');
    %KI = diag([1.0e+2 ,1.0e+3 , 1.0e+3].');
    KD = diag([1.0e+1 ,1.0e+1 , 1.0e+1].');
    
    % Set desired task space point
    % q_Tsdesired= [theta h_l   h_r ].';
    x_Ts_desired = [0.0, 1.36 , 1.36].';
    dx_Ts_desired = zeros(3,1);
    
    % Get measeurements
    qdata = x(1:7);
    dqdata = x(8:14);
    paramsdata = model.parameters.values;
    
%     % Get measeurements of only the joint coordinates.
%     qrdata =  qdata(nb+1:end);
%     dqrdata = dqdata(nb+1:end);
    

    % Task space EoM
    % lambda*ddq_Ts + mu + p = F
    
    % Get measurements
    xdata_Ts = model.controller.compute.x_Ts(paramsdata,qdata);
    dxdata_Ts = model.controller.compute.dx_Ts(paramsdata,qdata,dqdata);
    
    % Get Measurement of Generalized Jacobian
    Idata_J = model.controller.compute.I_J_g(paramsdata,qdata,dqdata);
    
    % Idata_J_bar=inv_Mdata*Idata_J'*lambdadata;
    lambdadata = model.controller.compute.lambda(paramsdata,qdata,dqdata);
    
    %errors
    error = x_Ts_desired - xdata_Ts;
    derror= dx_Ts_desired - dxdata_Ts;
    
    % Feedback
    %F_FB =directpath(KP,KD,error,derror,dxdata_Ts, lambdadata);
    %T_j_FB = Idata_J'*F_FB;
    
    F_FB = PD(KP,KD,error,derror,dxdata_Ts, lambdadata);
    
    % Feedforward
    F_FF = mu+p;
    
    % Collect terms
    T_j= Idata_J.' * (F_FB + F_FF);
    
end
function [F_FB] = PD(KP, KD, error, derror,dxdata_Ts, lambdadata)
    F_FB=KP*error+KD*derror;
end
function [F_FB] = directpath(KP, KD, error, derror,dxdata_Ts, lambdadata)
    v_max = 20;
    dx_Ts_desired_bar=diag(diag(KP)./diag(KD))*(error);
    nu = min(1, v_max/sqrt(dx_Ts_desired_bar'*dx_Ts_desired_bar));
    
    F_FB_desired = -KD*(dxdata_Ts-nu*dx_Ts_desired_bar);
    F_FB = lambdadata*F_FB_desired;
    % T_j_FB = Idata_J'*F_FB;
    
end

