% proNEu: A tool for rigid multi-body mechanics in robotics.
%
% Copyright (C) 2017  Marco Hutter, Christian Gehring, C. Dario Bellicoso,
% Vassilios Tsounis
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%
%   File:           planarleggedrobot_taskspace_pid_controller.m
%
%   Authors:        Manuel Breitenstein, bmanuel@ethz.ch
%
%   Date:           27/10/2017
%
%   Desciption:     Task-space PID controller.
%

function [ T_j ] = Copy_5_of_planarleggedrobot_taskspace_pid_controller(model,world,logger,t,x)
    % Number of bodies
    n=7;
    
    % Number of body coordinates
    nb=3;
    
    % Number of Task space coordinates
    nTs=3;
    
    % Number of simplification steps
    ns = 4;
    
    % Integrators
    persistent integrator;
    persistent counter;
    persistent h_q_Ts;
    persistent h_dq_Ts;
    persistent h_I_J;
    
    if isempty(counter)
        counter=0;
    end
    
    %Set gains
    %K= diag([K_theta,   K_hl,    K_hr ]
    KP = diag([1.0e+2 ,1.0e+3 , 1.0e+3].');
    %KI = diag([1.0e+2 ,1.0e+3 , 1.0e+3].');
    KD = diag([1.0e+1 ,1.0e+1 , 1.0e+1].');
    
    % Set desired task space point
    % q_Tsd=[theta h_l   h_r ].';
    q_Ts_desired = [0.0, 1.36 , 1.36].';
    % dq_Ts_desired = zeros(3,1);
    
    % Get measeurements
    qdata = x(1:7);
    dqdata = x(8:14);
    paramsdata = model.parameters.values;
    
    % Reset integrators
    if t <= 0.05
        integrator = zeros(3,1);
    end
    
    if counter < 1
        counter=counter+1;
        % [h_q_Ts, h_dq_Ts, h_I_J, h_lambda, h_mu, h_p].' = create_function_handles(model, world,logger,t,x);
        handles = create_function_handles(model, world,logger,t,x);
        h_q_Ts = handles{1,1};
        h_dq_Ts = handles{2,1};
        h_I_J = handles{3,1};
    end
    
    % Calculate task space EoM
    % lambda*ddq_Ts + mu + p = F
    
    % Get measurements
    qdata_Ts = h_q_Ts(paramsdata,qdata);
    dqdata_Ts = h_dq_Ts(paramsdata,qdata,dqdata);
    
    
    % Selection Matrix for joint generalized coordinates
    S_q = model.dynamics.compute.S_a(qdata,dqdata,[],[],paramsdata);
    
    
    Mdata_sys = model.dynamics.compute.M(qdata,dqdata,[],[],paramsdata);
    
    %inv_M_sys = simplify(eye(n)/M_sys, ns);
    inv_Mdata_sys = (eye(n)/Mdata_sys);
    
    Mdata = eye(size(S_q,1))/(S_q*inv_Mdata_sys*S_q');
    % M = inv((S_q*inv_M_sys*S_q'));
    
    inv_Mdata = eye(size(Mdata,1))/Mdata;
    
    bdata_sys = model.dynamics.compute.b(qdata,dqdata,[],[],paramsdata);
    gdata_sys = model.dynamics.compute.g(qdata,dqdata,[],[],paramsdata);
    
    % Dynamically consistent generelized inverse of selection matrix
    Sdata_q_bar=inv_Mdata_sys*S_q'*Mdata;
    
    % Coriolis forces of free floating system
    bdata= Sdata_q_bar'*bdata_sys;
    gdata= Sdata_q_bar'*gdata_sys;
    
    % Get Measurement of Generalized Jacobian
    Idata_J = h_I_J(paramsdata,qdata);
    lambdadata=eye(nTs)/(Idata_J*inv_Mdata*Idata_J');
    
    % Idata_J_bar=inv_Mdata*Idata_J'*lambdadata;
    
    %errors
    error = q_Ts_desired - qdata_Ts;
    %     for i = 2:3
    %      if error(i,1)<2
    %          error(i,1)=0;
    %      end
    %     end
    
    % derror = dq_Ts_desired - dqdata_Ts;
    
    % Feedback controller
    v_max = 20;
    dq_Ts_desired=diag(diag(KP)./diag(KD))*(error);
    nu = min(1, v_max/sqrt(dq_Ts_desired'*dq_Ts_desired));
    
    F_FB_desired = -KD*(dqdata_Ts-nu*dq_Ts_desired);
    F_FB = lambdadata*F_FB_desired;
    T_j_FB = Idata_J'*F_FB;
    
    %     Feedforward
    %     dIdata_J= h_dI_J(params,q,dq);
    %     T_j_FF = bdata - Idata_J'*lambdadata*dIdata_J*dqdata + gdata;
    T_j_FF= [0 0 0 0].';
    
    % Collect terms
    T_j= T_j_FB + T_j_FF;
    
end
function [handles] = create_function_handles(model,world,logger,t,x)
    proneu_info('Creating Taskspace function handles.');
    % Number of bodies
    n=7;
    
    % Number of body coordinates
    nb=3;
    
    % Number of Task space coordinates
    nTs=3;
    
    % Number of simplification steps
    ns = 4;
    
    % Get generalized coordinate
    q = model.dynamics.symbols.q(1:7);
    %    qr = q(nb+1:end);
    dq = model.dynamics.symbols.dq(1:7);
    %    dqr = dq(nb+1:end);
    params = model.parameters.symbols;
    
    T_bar_l = model.body(4).kinematics.symbols.T_IBi;
    T_bar_r = model.body(7).kinematics.symbols.T_IBi;
    
    
    % Symbolic coordinates in task space
    q_Ts = [q(3)-pi/2, q(1)-T_bar_l(3,4), q(1)-T_bar_r(3,4) ].';
    h_q_Ts  = matlabFunction(q_Ts, 'Vars',{params,q});
    dq_Ts= dAdt(q_Ts,q,dq);
    h_dq_Ts = matlabFunction(dq_Ts, 'Vars', {params, q, dq});
    
    %   ddq_Ts= dAdt(dq_Ts,q,dq)
    %   h_ddq_Ts = matlabFunction(ddq_Ts, 'Vars', {params, dq, ddq});
    
    
    % Mass/inertia matrix
    M_sys = model.dynamics.symbols.M;
    M_sys_bb=M_sys(1:nb,1:nb);
    inv_M_sys_bb = eye(nb)/M_sys_bb;
    M_sys_br=M_sys(1:nb,nb+1:end);
    
    
    % System Jacobian
    I_J_sys =simplify(jacobian(q_Ts,q), ns);
    I_J_sys_b = I_J_sys(:,1:nb);
    I_J_sys_r = I_J_sys(:,nb+1:end);
    
    % Generalized Jacobian
    I_J = I_J_sys_r - I_J_sys_b*inv_M_sys_bb*M_sys_br;
    %I_J = simplify(I_J_sys*Sdata_q_bar, ns);
    h_I_J = matlabFunction(I_J,'Vars',{params,q});
    %     dI_J=dAdt(I_J,q,dq);
    %     h_dI_J = matlabFunction(dI_J,'Vars',{params,q,dq});
    
    
    handles = {h_q_Ts, h_dq_Ts, h_I_J}.';
    
    
end
function [handles] = create_function_handlesold(model,world,logger,t,x)
    proneu_info('Creating Taskspace function handles.');
    % TODO delete if not needed
    %Number of bodies
    n=7;
    
    %Numbe of simplification steps
    ns = 4;
    
    % number of body coordinates
    nb=3;
    
    % Get generalized coordinate
    q = model.dynamics.symbols.q(1:7);
    qr = q(nb+1:end);
    dq = model.dynamics.symbols.dq(1:7);
    dqr = dq(nb+1:end);
    params = model.parameters.symbols;
    
    T_bar_l = model.body(4).kinematics.symbols.T_IBi;
    T_bar_r = model.body(7).kinematics.symbols.T_IBi;
    
    % Symbolic coordinates in Task space
    q_Ts = [q(3)-pi/2, q(1)-T_bar_l(3,4), q(1)-T_bar_r(3,4) ].';
    h_q_Ts  = matlabFunction(q_Ts, 'Vars',{params,q});
    dq_Ts= dAdt(q_Ts,q,dq);
    h_dq_Ts = matlabFunction(dq_Ts, 'Vars', {params, q, dq});
    %   ddq_Ts= dAdt(dq_Ts,q,dq)
    %   h_ddq_Ts = matlabFunction(ddq_Ts, 'Vars', {params, dq, ddq});
    
    % Selection Matrix for joint generalized coordinates
    S_q = [zeros(4,3) eye(4)];
    
    
    % Dynamic parameters
    M_sys = model.dynamics.symbols.M;
    %inv_M_sys = simplify(eye(n)/M_sys, ns);
    inv_M_sys = (eye(n)/M_sys);
    
    M = simplify(eye(size(S_q,1))/(S_q*inv_M_sys*S_q'),ns);
    % M = inv((S_q*inv_M_sys*S_q'));
    
    inv_M = simplify(eye(size(M,1))/M, ns);
    h_inv_M = matlabFunction(inv_M,'Vars', {params, qr, dqr});
    
    b_sys = model.dynamics.symbols.b;
    g_sys = model.dynamics.symbols.g;
    
    %     Schur complements
    %     bb = b_sys(1:nb);
    %     br = b_sys(nb+1:end);
    %     gb = g_sys(1:nb);
    %     gr = g_sys(nb+1:end);
    %
    %     M_sys_bb=M_sys(1:nb,1:nb);
    %     M_sys_rb=M_sys(nb+1:end,1:nb);
    
    %     inv_M_sys_bb = eye(nb)/M_sys_bb;
    
    % Dynamically consistent generelized inverse of selection matrix
    S_q_bar=simplify(inv_M_sys*S_q'*M, ns);
    
    % Coriolis forces of free floating system
    b= S_q_bar'*b_sys;
    g= S_q_bar'*g_sys;
    
    %   g= gr - M_sys_rb*inv_M_sys_bb*gb;
    
    h_b=matlabFunction(b,'Vars',{params,q,dq});
    h_g=matlabFunction(g,'Vars',{params,q});
    
    
    % System Jacobian
    I_J_sys =simplify(jacobian(q_Ts,q), ns);
    
    
    
    
    % Generalized Jacobian
    I_J = simplify(I_J_sys*S_q_bar, ns);
    h_I_J = matlabFunction(I_J,'Vars',{params,q});
    
    %    % TODO delete or add following section
    %     dummy = simplify((I_J*inv_M*I_J'), ns);
    %
    %     % Operational space mass/inertia
    %     lambda=eye(n)/dummy;
    %
    %     % dynamically consistent generalized Jacobian
    %      I_J_bar=inv_M*I_J'*lambda;
    %
    %     %operational space coriolis and gravity terms
    %     mu = b* I_J_bar;
    %     p = g *I_J_bar;
    %
    %     h_lambda = matlabfunction(lambda,'Vars', q_TS);
    %
    %
    %     handles = [h_q_Ts, h_dq_Ts, h_I_J, h_lambda ,h_mu, h_p].'
    
    handles = {h_q_Ts, h_dq_Ts, h_I_J , h_inv_M, h_b, h_g }.';
    
    
end

