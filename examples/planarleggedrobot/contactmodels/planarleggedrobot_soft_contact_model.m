% proNEu: A tool for rigid multi-body mechanics in robotics.
%
% Copyright (C) 2017  Marco Hutter, Christian Gehring, C. Dario Bellicoso,
% Vassilios Tsounis
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%
%   File:           planarleggedrobot_soft_contact_model.m
%
%   Authors:        This is a copy of the monopod_soft_contact_model.m of Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           16/10/2017
%
%   Desciption:     A ground-force contact model using soft contacts and a
%                   point foot assumption.
%

function [ W_F ] = planarleggedrobot_soft_contact_model(model,logger,t,x)

% Set simulation data
qdata = x(1:7);
dqdata = x(8:14);
mparams = model.parameters.values;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Left  Foot%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%     % Extract kinematic information of the left foot.
%     T_c = model.body(4).kinematics.compute.T_IBi(qdata,[],[],mparams);
%     J_c = model.body(4).kinematics.compute.I_J_IBi(qdata,[],[],mparams);
%     
%     % Compliant contact model
%     %
%     
%     % Parameters
%     persistent first_contact_detected_l;
%     persistent is_slipping_l;
%     persistent lambda_N_l;
%     persistent lambda_T_l;
%     persistent P_0_l;
%     persistent P_k_l;
%     persistent Q_k_l;
%     persistent Q_0_l;
%     
%     % Constants
%     z_Floor = 0;
%     Tol_gamma = 1.0e-6;
%     mu = 0.4;
%     c_N = 1.0e+5;
%     d_N = 1.0e+4;
%     c_T = 1.0e+5;
%     d_T = 1.0e+4;
%     
%     % Initialize persistent variables
%     if (t == 0)
%         first_contact_detected_l = true;
%         is_slipping_l = false;
%         lambda_N_l = 0;
%         lambda_T_l = [0 0].';
%         P_0_l = 0;
%         P_k_l = 0;
%         Q_k_l = [0 0].';
%         Q_0_l = [0 0].';
%     end
%     
%     % Get foot coordinates
%     x_Fk = T_c(1,4);
%     y_Fk = T_c(2,4);
%     z_Fk = T_c(3,4);
%     
%     % Normal gap function for contact detection
%     g_N_check = z_Fk - z_Floor;
%     
%     % Check for closed contacts
%     if g_N_check <= 0
%         
%         % Check for first contact point
%         if first_contact_detected_l == true
%             P_0_l = z_Fk;
%             Q_0_l = [x_Fk y_Fk].';
%             first_contact_detected_l = false;
%         end
%         
%         % Update contact compliance variables
%         P_k_l = z_Fk;
%         Q_k_l = [x_Fk y_Fk].';
%         
%         % Compute gap functions
%         g_N = P_k_l - P_0_l;
%         g_T = Q_k_l - Q_0_l;
%         
%         % Relative gap velocity
%         gamma_c = J_c*dqdata;
%         gamma_N = gamma_c(3);
%         gamma_T = gamma_c(1:2);
%         
%         % Compliant contact algorithm, explained in [1]
%         lambda_N_l = max(-c_N*g_N - d_N*gamma_N,0);
%         if is_slipping_l == true;
%             if (norm(gamma_T,2) <= Tol_gamma) || (norm(lambda_T_l,2) <= mu*lambda_N_l)
%                 is_slipping_l = false;
%                 Q_0_l = [x_Fk y_Fk].';
%             end
%         else
%             lambda_T_l = -c_T*g_T - d_T*gamma_T;
%             if norm(lambda_T_l,2) >= mu*lambda_N_l
%                 lambda_T_l = -mu*lambda_N_l*(gamma_T/abs(gamma_T));
%                 is_slipping_l = true;
%             end
%         end
%     else
%         first_contact_detected_l = true;
%         % Forces are zero
%         lambda_N_l = 0;
%         lambda_T_l = [0 0].';
%     end
%     
%     % Set results
%     F_Fx = lambda_T_l(1);
%     F_Fy = 0;           %%TODO delete this comment: I Deleted this force so that feet are only subject to planra forces
%     F_Fz = lambda_N_l;
%     T_Fx = 0.0;
%     T_Fy = 0.0;
%     T_Fz = 0.0;
%     
% 
%         W_F = [F_Fx F_Fy F_Fz T_Fx T_Fy T_Fz].';
%     
%       
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Right  Foot%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%     % Extract kinematic information of the right foot.
%     T_c = model.body(7).kinematics.compute.T_IBi(qdata,[],[],mparams);
%     J_c = model.body(7).kinematics.compute.I_J_IBi(qdata,[],[],mparams);
%     
%     % Compliant contact model
%     %
%     
%     % Parameters
%     persistent first_contact_detected_r;
%     persistent is_slipping_r;
%     persistent lambda_N_r;
%     persistent lambda_T_r;
%     persistent P_0_r;
%     persistent P_k_r;
%     persistent Q_k_r;
%     persistent Q_0_r;
%     
%     % Constants
%     z_Floor = 0;
%     Tol_gamma = 1.0e-6;
%     mu = 0.4;
%     c_N = 1.0e+5;
%     d_N = 1.0e+4;
%     c_T = 1.0e+5;
%     d_T = 1.0e+4;
%     
%     % Initialize persistent variables
%     if (t == 0)
%         first_contact_detected_r = true;
%         is_slipping_r = false;
%         lambda_N_r = 0;
%         lambda_T_r = [0 0].';
%         P_0_r = 0;
%         P_k_r = 0;
%         Q_k_r = [0 0].';
%         Q_0_r = [0 0].';
%     end
%     
%     % Get foot coordinates
%     x_Fk = T_c(1,4);
%     y_Fk = T_c(2,4);
%     z_Fk = T_c(3,4);
%     
%     % Normal gap function for contact detection
%     g_N_check = z_Fk - z_Floor;
%     
%     % Check for closed contacts
%     if g_N_check <= 0
%         
%         % Check for first contact point
%         if first_contact_detected_r == true
%             P_0_r = z_Fk;
%             Q_0_r = [x_Fk y_Fk].';
%             first_contact_detected_r = false;
%         end
%         
%         % Update contact compliance variables
%         P_k_r = z_Fk;
%         Q_k_r = [x_Fk y_Fk].';
%         
%         % Compute gap functions
%         g_N = P_k_r - P_0_r;
%         g_T = Q_k_r - Q_0_r;
%         
%         % Relative gap velocity
%         gamma_c = J_c*dqdata;
%         gamma_N = gamma_c(3);
%         gamma_T = gamma_c(1:2);
%         
%         % Compliant contact algorithm, explained in [1]
%         lambda_N_r = max(-c_N*g_N - d_N*gamma_N,0);
%         if is_slipping_r == true;
%             if (norm(gamma_T,2) <= Tol_gamma) || (norm(lambda_T_r,2) <= mu*lambda_N_r)
%                 is_slipping_r = false;
%                 Q_0_r = [x_Fk y_Fk].';
%             end
%         else
%             lambda_T_r = -c_T*g_T - d_T*gamma_T;
%             if norm(lambda_T_r,2) >= mu*lambda_N_r
%                 lambda_T_r = -mu*lambda_N_r*(gamma_T/abs(gamma_T));
%                 is_slipping_r = true;
%             end
%         end
%     else
%         first_contact_detected_r = true;
%         % Forces are zero
%         lambda_N_r = 0;
%         lambda_T_r = [0 0].';
%     end
%     
%     % Set results
%     F_Fx = lambda_T_r(1);
%     F_Fy = 0;           %%TODO delete this comment: I Deleted this force so that feet are only subject to planar forces
%     F_Fz = lambda_N_r;
%     T_Fx = 0.0;
%     T_Fy = 0.0;
%     T_Fz = 0.0;
%     
%     %Update Wrench 
%     %Should be the same order as the environment forces and torques in the dynamics
%     W_F = [W_F.' F_Fx F_Fy F_Fz T_Fx T_Fy T_Fz].';
%    
%     

%TODO Delet next line or delete al of above

W_F= [0 0 0 0 0 0 0 0 0 0 0 0].';


end

%% References
%
% [1]   C. Gehring, R. Diethelm, R. Siegwart, G. N�utzi, R. Leine. "An Evaluation
%       of Moreau�s Time-Stepping Scheme for the Simulation of a Legged Robot." 2014
%

%%
% EOF
