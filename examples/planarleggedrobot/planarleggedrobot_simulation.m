% proNEu: A tool for rigid multi-body mechanics in robotics.
% 
% Copyright (C) 2017  Marco Hutter, Christian Gehring, C. Dario Bellicoso,
% Vassilios Tsounis
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
% p
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%% TODO
%   File:           planarleggedrobot_dynamics.m
%
%   Authors:        Manuel Breitenstein, bmanuel@ethz.ch
%
%   Date:           17/10/2017
%
%   Desciption:     Analytical derivation of the EoM for a planar 5 link legged robot 
%                   in a low gravity environment.
%                   File is based on the monopod example of Vassilios
%                   Tsounis, tsounisv@ethz.ch.
%

% Optional calls for workspace resetting/cleaning
close all; clear all; clear classes; clc;

% Load the Model Data

load('/home/manu/git/proneu/examples/planarleggedrobot/PlanarleggedrobotModel.mat')
load('PlanarleggedrobotWorld.mat');

%% Create a Controller

% Generate a robot controller using an external implementation
% controller = RobotController(@planarleggedrobot_jointspace_pid_controller);
 controller = RobotController(@planarleggedrobot_taskspace_pid_controller);
% controller = RobotController(@planarleggedrobot_jointspace_impedance_controller);
% controller = RobotController(@planarleggedrobot_jointspace_inverse_dynamics_controller);
% controller = RobotController(@planarleggedtrobot_simple_controller);

%% Generate the Simulation Environment

% Create the robot simulation engine
robotsim = RobotSimulator(robotmdl, controller , worldmdl, 'solver', 'fixedstep');


% Data Plots
% robotsim.scope.open();

%% Set the Model Parameters

% System parameter values
% Get parameter symbols by using robotmdl.parameters.symbols.'
%
% params= [ d_Bx, d_By, d_Bz, d_Llx, d_Lly, d_Llz, d_Lrx, d_Lry, d_Lrz, d_Ulx, d_Uly, d_Ulz, d_Urx, d_Ury, d_Urz, grav, h_B, l_B, l_Ll, l_Lr, l_Ul, l_Ur, m_B, m_Ll, m_Lr, m_Ul, m_Ur, r_Fl, r_Fr, r_Ll, r_Lr, r_Ul, r_Ur, w_B]
mparams =  [0.0   0.0   1.0   0.0    0.0   -0.5     0.0    0.0    0.5    0.0   0.0    -0.5    0.0    0.0    0.5   0.0    2.0  0.1  1.0   1.0   1.0   1.0   3.0   0.2  0.2   0.2   0.2   0.07  0.07  0.05  0.05 0.05  0.05  0.1].';

% Store the parameters
robotmdl.parameters.values = mparams;

%% Set the system's initial conditions

% Get the symbolic list by robotmdl.dynamics.symbols.q and the corresponding derivatives
% xinit =[z_B, x_B, phi_B, phi_Ul, phi_Ll, phi_Ur, phi_Lr, dz_B, dx_B  dphi_B  dphi_Ul dpi_Ll dphi_Ur dphi_Lr]
xinit =  [2.0  -1  pi/4*1.5  -pi/6   -pi/6   pi/6    pi/6      0.0   0.0    0.00     0.0     0.0    0.0      0.0].';

%% Check if Dimensions of parameters and initial conditions are correct


if size(mparams,1)~=size(robotmdl.parameters.symbols,1)
   error('Error. \nDimensions mismatch \nDimension of parameter vector in the simulation is not equal to the number of parameters implemented by the dynamics.\nDimension of parameter Vector: %d\nNumber of parameters in dynamics: %d\n',size(mparams,1), size(robotmdl.parameters.symbols,1));
end

if size(xinit,1)~=size(robotmdl.dynamics.symbols.q,1)*2
   error('Error. \nDimensions mismatch \nDimension of variable vector in the simulation is not equal to double the number of variables implemented by the dynamics.\nDimension of parameter Vector: %d\nNumber of variables in dynamics: %d\n',size(xinit,1),size(robotmdl.dynamics.symbols.q,1)*2);
end

%% Create the Visualizations

% Generate 3D Visualization instance
robotviz = RobotVisualization(robotmdl, worldmdl);
robotviz.open();
robotviz.load();
%set(gcf, 'Position', get(0, 'Screensize'));

% Give the simulator access to the 3D visualization
robotsim.setvisualizer(robotviz);

%% Run Simulation

% Set the system parameters used by the simulation
robotsim.setup('xinit', xinit);

% Simulation time configurations
simconf.tstart  = 0;
simconf.tstop   = 20;
simconf.tstep   = 1e-3;
simconf.fps     = 20.0;

% Execute the simlation engine
robotsim.run(simconf);

%%
% EOF
